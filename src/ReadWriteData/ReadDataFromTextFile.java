package ReadWriteData;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class ReadDataFromTextFile {
    public static void main(String[] args) throws IOException {
        // Approach 1 : Using FileReader BufferedReader
        FileReader fr = new FileReader("/Users/amitjangra/Desktop/Kafka Notes.rtf");
        BufferedReader br = new BufferedReader(fr);
        String str;
        while ((str=br.readLine())!=null) {
            System.out.println(str);
        }
        br.close();

        // Approach 2 : Using Scanner & File
        File file = new File("/Users/amitjangra/Desktop/Kafka Notes.rtf");
        Scanner sc = new Scanner(file);
        while (sc.hasNextLine()) {
            System.out.println(sc.nextLine());
        }

        // Approach 3
        sc.useDelimiter("//Z");
        System.out.println(sc.next());
    }
}
