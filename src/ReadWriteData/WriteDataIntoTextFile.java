package ReadWriteData;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class WriteDataIntoTextFile {
    public static void main(String[] args) throws IOException {
        FileWriter fw = new FileWriter("/Users/amitjangra/Desktop/Test123.txt");
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write("Java");
        bw.write("Programming");
        System.out.println("finish");
        bw.close();
    }
}
