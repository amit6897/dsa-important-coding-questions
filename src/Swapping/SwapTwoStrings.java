package Swapping;

public class SwapTwoStrings {
    // SWAP WITHOUT USING THIRD VARIABLE
    public static void main(String[] args) {
        String str1 = "java";
        String str2 = "guides";

        System.out.println(" before swapping two strings ");
        System.out.println(" s1 => " + str1);
        System.out.println(" s2 => " + str2);

        // concat str1 and str2 and store in str1
        str1 = str1 + str2;

        // store initial value of str1 into str2
        str2 = str1.substring(0, str1.length()-str2.length());

        // store initial value of str2 into str1
        str1 = str1.substring(str2.length());

        System.out.println(" after swapping two strings ");
        System.out.println(" s1 => " + str1);
        System.out.println(" s2 => " + str2);
    }

    /* SWAP USING THIRD VARIABLE
    public static void main(String[] args) {
        String str1 = "java";
        String str2 = "guides";

        System.out.println(" before swapping two strings ");
        System.out.println(" s1 => " + str1);
        System.out.println(" s2 => " + str2);

        String temp = str1;
        str1 = str2;
        str2 = temp;

        System.out.println(" after swapping two strings ");
        System.out.println(" s1 => " + str1);
        System.out.println(" s2 => " + str2);
    }
     */
}
