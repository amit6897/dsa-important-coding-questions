package SearchingAlgorithms.BinarySearch.OneDimensionalArray;

import java.util.Arrays;

public class First_and_Last_Position_of_Element {
    static int[] searchRange(int[] nums, int target) {
        int[] ans = {-1, -1};
        int start = search(nums, target, true);
        int last = search(nums, target, false);
        ans[0] = start;
        ans[1] = last;
        return ans;
    }

    static int search(int[] nums, int target, boolean firstStartIndex) {
        int ans = -1;
        int start = 0;
        int end = nums.length-1;

        while(start <= end) {
            int mid = start + (end-start) / 2;
            if (target < nums[mid]) {
                end = mid - 1;
            } else if (target > nums[mid]) {
                start = mid + 1;
            } else {
                ans = mid;
                if (firstStartIndex) {
                    end = mid -1;
                } else {
                    start = mid + 1;
                }
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        int[] nums = {5,7,7,7,7,8,8,10,12};
        int target = 10;
        //int[] answer = searchRange(nums, target);
        //System.out.println(answer);
        System.out.println(Arrays.toString(searchRange(nums, target)));
    }
}
