package SearchingAlgorithms.BinarySearch.OneDimensionalArray;

public class Ceiling_and_Floor_In_Array {
    static int findCeiling(int[] arr, int target) {
        // if the target is greater than the greatest number in array
        if (target > arr[arr.length-1]) {
            return -1;
        }
        int start = 0;
        int end = arr.length-1;
        while (start <= end) {
            int mid = start + (end - start) / 2;
            if (target < arr[mid]) {
                end = mid - 1;
            } else if (target > arr[mid]) {
                start = mid + 1;
            } else {
                return mid;
            }
        }
        return arr[start];
    }

    static int findFloor(int[] arr, int target) {
        int start = 0;
        int end = arr.length-1;
        while (start <= end) {
            int mid = start + (end - start) / 2;
            if (target < arr[mid]) {
                end = mid - 1;
            } else if (target > arr[mid]) {
                start = mid + 1;
            } else {
                return mid;
            }
        }
        return arr[end];
    }

    public static void main(String[] args) {
        int[] arr = {2, 3, 5, 9, 14, 16,18};
        int target = 1;
        int ceil = findCeiling(arr, target);
        int floor = findFloor(arr, target);
        System.out.println("Ceiling is : " + ceil);
        System.out.println("Floor is : " + floor);
    }
}
