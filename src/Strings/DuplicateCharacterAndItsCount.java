package Strings;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DuplicateCharacterAndItsCount {
    public static void main(String[] args) {
        //String input = "JavaJavaEE";
        String input = "manojpandey";
        Map<Character, Long> result = input.chars().mapToObj(c->(char)c)
                .collect(Collectors.groupingBy(c->c, Collectors.counting()));
        result.forEach((k,v)-> {
            if (v > 1) {
                System.out.println(k+" : "+v);
            }
        });

        /* It will only give the duplicate characters not the count
        String str = "manojpandey";
        List<String> duplicateElements = Arrays.stream(str.split(""))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .filter(x->x.getValue()>1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        System.out.println(duplicateElements);
        */
    }
}
