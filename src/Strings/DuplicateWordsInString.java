package Strings;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DuplicateWordsInString {
    static void duplicate(String str) {
        final String[] words = str.split(" ");
        final Map<String, Integer> wordCount = new HashMap<>();
        for (String word: words) {
            if (wordCount.containsKey(word)) {
                wordCount.put(word, wordCount.get(word)+1);
            } else {
                wordCount.put(word, 1);
            }
        }
        final Set<String> wordsInString = wordCount.keySet();
        for (String word: wordsInString) {
            if (wordCount.get(word) > 1) {
                System.out.println(word+" : "+wordCount.get(word));
            }
        }
    }

    public static void main(String[] args) {
        duplicate("java guides java");
    }
}
