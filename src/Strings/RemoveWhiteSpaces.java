package Strings;

public class RemoveWhiteSpaces {
    public static void main(String[] args) {
        String str = "java  python c++   ds";
        str = str.replaceAll("\\s", "");
        System.out.println(str);
    }
}
