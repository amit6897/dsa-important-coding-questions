package Strings;

public class ReverseEachWord {
    public static void main(String[] args) {
        /* Approach 1
        String str = "Welcome To Java";
        String[] words = str.split(" ");
        String reverseString = "";
        for (String w: words) {
            String reverseWord = "";
            for (int i=w.length()-1; i>=0; i--) {
                reverseWord = reverseWord+w.charAt(i);
            }
            reverseString = reverseString + reverseWord+" ";
        }
        System.out.println(reverseString);      */

        // Approach 2
        String str = "Welcome To Java";
        String[] word = str.split("\\s");
        String reverseWord = "";
        for (String w: word) {
            StringBuilder sb = new StringBuilder(w);
            sb.reverse();
            reverseWord=reverseWord+sb.toString()+" ";
        }
        System.out.println(reverseWord);
    }
}
