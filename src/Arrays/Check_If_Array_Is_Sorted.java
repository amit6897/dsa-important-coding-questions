package Arrays;

public class Check_If_Array_Is_Sorted {
    // We will check by Single Traversal
    // T.C -> O(N)      S.C -> O(1)
    public static void main(String[] args) {
        int arr[] = {1, 2, 2, 3, 3, 4};
        System.out.println(isSort(arr));
    }

    static boolean isSort(int[] arr) {
        for (int i=1; i< arr.length; i++) {
            if (arr[i] < arr[i-1])
                return false;
        }
        return true;
    }
}
