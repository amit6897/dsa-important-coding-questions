package Arrays.ArrayRotation;

public class Left_Rotate_By_D_Places {
    
    // BRUTE FORCE APPROACH
    /*
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7};
        int n = arr.length;
        int d = 10;
        leftRotate(arr, n, d);
        for (int i=0; i<n; i++) {
            System.out.print(arr[i]+" ");
        }
    }

    static void leftRotate(int[] arr, int n, int d) {
        if (n==0) return;
        d = d % n;
        if (d==0) return;
        int temp[] = new int[d];
        for (int i=0; i<d; i++) {
            temp[i] = arr[i];
        }
        for (int i=d; i<n; i++) {
            arr[i-d] = arr[i];
        }
        for (int i=n-d; i<n; i++) {
            arr[i] = temp[i-(n-d)];
        }
    }
    */
}
