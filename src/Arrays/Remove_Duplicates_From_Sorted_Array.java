package Arrays;

import java.util.HashSet;
import java.util.Set;

public class Remove_Duplicates_From_Sorted_Array {
    // OPTIMAL APPROACH - Using 2 pointers
    // T.C -> O(N)      S.C -> O(1)
    static int remove(int[] arr, int n) {
        int i=0;
        for (int j=1; j<n; j++) {
            if (arr[j] != arr[i]) {
                arr[i+1] = arr[j];
                i++;
            }
        }
        return i+1;
    }

    public static void main(String[] args) {
        int arr[] = {1, 1, 2, 2, 2, 3, 3};
        int n = arr.length;
        int k = remove(arr, n);
        for (int i=0; i<k; i++) {
            System.out.print(arr[i]+" ");
        }
    }


    // BRUTE FORCE APPROACH - Using Set
    // T.C -> O(NlogN)+O(N)     S.C -> O(N)

    /*  1. Declare a HashSet
        2. Run a for loop from starting to end
        3. Put every element of the array in the set
        4. Store size of the set in a variable K
        5. Now put all elements of the set in the array from the starting of the array
        6. Return K

    static int remove(int[] arr, int n) {
        Set<Integer> set = new HashSet<>();
        for (int i=0; i<n; i++) {
            set.add(arr[i]);
        }
        int k = set.size();
        int index = 0;
        for (int element: set) {
            arr[index++] = element;
        }
        return k;
    }

    public static void main(String[] args) {
        int arr[] = {1, 1, 2, 2, 2, 3, 3};
        int n = arr.length;
        int k = remove(arr, n);
        for (int i=0; i<k; i++) {
            System.out.print(arr[i]+" ");
        }
    }
    */
}
